package com.ftw.tugasbinar7apr

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide

class MainAdapter(val data: MutableList<Makanan>) :
    RecyclerView.Adapter<MainAdapter.MainViewHolder>() {
    private lateinit var listener: OnClickListener

    fun setOnClickListener(listener: OnClickListener) {
        this.listener = listener
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MainViewHolder {
        val v = LayoutInflater.from(parent.context).inflate(R.layout.item_view, parent, false)
        return MainViewHolder(v)
    }

    override fun getItemCount(): Int = data.size

    override fun onBindViewHolder(holder: MainViewHolder, position: Int) {
        holder.bind(data[position])
        holder.itemView.setOnClickListener {
            listener.let {
                it.onClick(data[position],position)
            }
        }
    }

    class MainViewHolder(val view: View) : RecyclerView.ViewHolder(view) {
        val image = view.findViewById<ImageView>(R.id.img_item)
        val title = view.findViewById<TextView>(R.id.tv_title)
        val price = view.findViewById<TextView>(R.id.tv_price)

        fun bind(makanan: Makanan) {
            Glide.with(view.context).load(makanan.image).into(image)
            title.text = makanan.title
            price.text = makanan.price
        }
    }

    interface OnClickListener {
        fun onClick(makanan: Makanan, position: Int)
    }
}