package com.ftw.tugasbinar7apr

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import androidx.recyclerview.widget.GridLayoutManager
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val data = mutableListOf<Makanan>(
            Makanan("https://cdn0-production-images-kly.akamaized.net/wIwQrQDvUCGS92etq3exM2SfNrQ=/640x360/smart/filters:quality(75):strip_icc():format(jpeg)/kly-media-production/medias/1253731/original/213c98558a7233f0f923c91df2b986f9beef-curry-recipe-stew-beef-in-coconut-curry.jpg","Rendang","Rp 5000"),
            Makanan("https://cdn0-production-images-kly.akamaized.net/wIwQrQDvUCGS92etq3exM2SfNrQ=/640x360/smart/filters:quality(75):strip_icc():format(jpeg)/kly-media-production/medias/1253731/original/213c98558a7233f0f923c91df2b986f9beef-curry-recipe-stew-beef-in-coconut-curry.jpg","Nasi Goreng","Rp 10000"),
            Makanan("https://cdn0-production-images-kly.akamaized.net/wIwQrQDvUCGS92etq3exM2SfNrQ=/640x360/smart/filters:quality(75):strip_icc():format(jpeg)/kly-media-production/medias/1253731/original/213c98558a7233f0f923c91df2b986f9beef-curry-recipe-stew-beef-in-coconut-curry.jpg","Ayam Goreng","Rp 10000"),
            Makanan("https://cdn0-production-images-kly.akamaized.net/wIwQrQDvUCGS92etq3exM2SfNrQ=/640x360/smart/filters:quality(75):strip_icc():format(jpeg)/kly-media-production/medias/1253731/original/213c98558a7233f0f923c91df2b986f9beef-curry-recipe-stew-beef-in-coconut-curry.jpg","Bakwan","Rp 8000"),
            Makanan("https://cdn0-production-images-kly.akamaized.net/wIwQrQDvUCGS92etq3exM2SfNrQ=/640x360/smart/filters:quality(75):strip_icc():format(jpeg)/kly-media-production/medias/1253731/original/213c98558a7233f0f923c91df2b986f9beef-curry-recipe-stew-beef-in-coconut-curry.jpg","Lele Bakar","Rp 7000"),
            Makanan("https://cdn0-production-images-kly.akamaized.net/wIwQrQDvUCGS92etq3exM2SfNrQ=/640x360/smart/filters:quality(75):strip_icc():format(jpeg)/kly-media-production/medias/1253731/original/213c98558a7233f0f923c91df2b986f9beef-curry-recipe-stew-beef-in-coconut-curry.jpg","Indomi","Rp 15000")
        )

        val adapterMain = MainAdapter(data)
        rv_main.layoutManager = GridLayoutManager(baseContext,3)
        rv_main.adapter = adapterMain

        adapterMain.setOnClickListener(object: MainAdapter.OnClickListener{
            override fun onClick(makanan: Makanan, position: Int) {
                Toast.makeText(baseContext,position.toString(),Toast.LENGTH_LONG).show()
            }

        })
    }
}
